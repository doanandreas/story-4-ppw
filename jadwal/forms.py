from django import forms

# Create your models here.
class JadwalKegiatan(forms.Form):
    # Nama kegiatan
    namaKegiatan = forms.CharField(label='Nama Kegiatan' ,max_length=200, initial="Nama kegiatan")

    # Tempat kegiatan
    tempatKegiatan = forms.CharField(label='Tempat kegiatan', max_length=100, initial="Tempat kegaitan")

    # Waktu kegiatan (tanggal, jam)
    # nanti kalo udah bener hari bisa diambil pake datetime python
    waktuKegiatan = forms.DateTimeField(
        label='Waktu kegiatan',

        # Ganti widget default ke widget Bootstrap
        widget = forms.DateTimeInput(attrs = {
            'class': 'form-control datetimepicker-input',
            'data-target': '#datetimepicker1',
        }),

        initial='Waktu kegiatan'
    )

    # Kategori kegiatan
    KATEGORI = [('Safe', 'Safe'), ('Urgent', 'Urgent'), ('Important', 'Important')]
    kategoriKegiatan = forms.ChoiceField(choices=KATEGORI)


