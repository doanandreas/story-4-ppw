from django.contrib import admin
from .models import JadwalModel

# Register your models here.
class JadwalModelAdmin(admin.ModelAdmin):
    pass

admin.site.register(JadwalModel, JadwalModelAdmin)