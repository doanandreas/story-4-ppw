from django.shortcuts import render
from .forms import JadwalKegiatan
from .models import JadwalModel

# Create your views here.
def index(request):
    jadwal = JadwalKegiatan()
    semuaKegiatan = JadwalModel.objects.all()
    
    argument = {
        'formJadwal' : jadwal,
        'semuaKegiatan' : semuaKegiatan,
    }

    return render(request, 'jadwal/index.html', argument)

def terima(request):
    jadwal = JadwalKegiatan()
    isianNamaKegiatan = request.POST['namaKegiatan']
    isianTempatKegiatan = request.POST['tempatKegiatan']
    isianWaktuKegiatan = request.POST['waktuKegiatan']
    isianKategoriKegiatan = request.POST['kategoriKegiatan']

    temp = JadwalModel(
        namaKegiatan = isianNamaKegiatan, 
        tempatKegiatan = isianTempatKegiatan, 
        waktuKegiatan = isianWaktuKegiatan,
        kategoriKegiatan = isianKategoriKegiatan,
    )

    temp.save()

    semuaKegiatan = JadwalModel.objects.all()
    
    argument = {
        'formJadwal' : jadwal,
        'semuaKegiatan' : semuaKegiatan,
    }

    return render(request, 'jadwal/index.html', argument)