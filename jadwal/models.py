from django.db import models

class JadwalModel(models.Model):
    namaKegiatan = models.CharField('Nama Kegiatan', max_length=200, help_text="Nama kegiatan yang dilakukan")
    tempatKegiatan = models.CharField('Tempat Kegiatan', max_length=100, help_text="Tempat kegiatan dilakukan")
    waktuKegiatan = models.DateTimeField('Waktu Kegiatan')
    kategoriKegiatan = models.CharField('Kategori Kegiatan', max_length=10)

    def __str__(self):
        return self.namaKegiatan
