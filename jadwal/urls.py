from django.urls import path
from . import views

app_name = 'jadwal'

urlpatterns = [
    path('', views.index, name="index"),
    path('terima/', views.terima, name="terima"),
]